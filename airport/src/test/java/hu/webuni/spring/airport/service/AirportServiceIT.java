package hu.webuni.spring.airport.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import hu.webuni.spring.airport.model.Airport;
import hu.webuni.spring.airport.model.Flight;
import hu.webuni.spring.airport.repository.AirportRepository;
import hu.webuni.spring.airport.repository.FlightRepository;

@SpringBootTest
@AutoConfigureTestDatabase
public class AirportServiceIT {

	@Autowired
	AirportService airportService;

	@Autowired
	AirportRepository airportRepository;

	@Autowired
	FlightRepository flightRepository;

	/*
	 * ez a metódus lefut minden teszt előtt, kitöröljük a teszteseteket nehogy a
	 * tesztmetódus azért bukjon el, mert ,,szemét" van a tesztadatbázisban
	 */
	@BeforeEach
	public void init() {
		flightRepository.deleteAll();
		airportRepository.deleteAll();
	}

	private long createAirport(String name, String iata) {
		return airportRepository.save(new Airport(name, iata)).getId();
	}

	private long createFlight(String flightNumber, long takoff, long landing, LocalDateTime dateTime) {
		return airportService.createFlight(flightNumber, takoff, landing, dateTime).getId();
	}

	@Test
	void testCreateFlight() throws Exception {
		String flightNumber = "AAA-01";
		long takeoff = createAirport("airport1", "iata1");
		long landing = createAirport("airport2", "AP2");
		LocalDateTime dateTime = LocalDateTime.now();
		long flightId = createFlight(flightNumber, takeoff, landing, dateTime);

		Optional<Flight> savedFlightOptional = flightRepository.findById(flightId);
		assertThat(savedFlightOptional).isNotEmpty();
		assertThat(savedFlightOptional.get().getFlightNumber()).isEqualTo(flightNumber);
		assertThat(savedFlightOptional.get().getTakeoff().getId()).isEqualTo(takeoff);
		assertThat(savedFlightOptional.get().getLanding().getId()).isEqualTo(landing);
//		assertThat(savedFlightOptional.get().getTakeoffTime()).isEqualTo(dateTime); //eltero pontossagu db es java
		assertThat(savedFlightOptional.get().getTakeoffTime()).isCloseTo(dateTime, within(1, ChronoUnit.MICROS));
	}

	@Test
	void testFindFlightsByExample() throws Exception {
		long airport1Id = createAirport("airport1", "iata1");
		long airport2Id = createAirport("airport2", "iata2");
		long airport3Id = createAirport("airport3", "2iata");
		long airport4Id = createAirport("airport4", "3iata");
		LocalDateTime takeoff = LocalDateTime.of(2021, 4, 23, 8, 0, 0);
		long flight1 = createFlight("ABC123", airport1Id, airport3Id, takeoff); // illeszkedik minden
		long flight2 = createFlight("ABC1234", airport2Id, airport3Id, takeoff.plusHours(2));
		long flight3 = createFlight("BC123", airport1Id, airport3Id, takeoff); // name nem illeszkedik
		long flight4 = createFlight("ABC123", airport1Id, airport3Id, takeoff.plusDays(1)); // túl sok nap
		createFlight("ABC123", airport3Id, airport3Id, takeoff.plusDays(1)); // ez nem lesz benne

		Flight example = new Flight();
		example.setFlightNumber("ABC123");
		example.setTakeoffTime(takeoff);
		example.setTakeoff(new Airport("sasa", "iata"));		
		System.out.println("AirportServiceIT > testFindFlightsByExample() > example: " + example.toString());
		
		List<Flight> foundFlights = this.airportService.findFlightsByExample(example);
		assertThat(foundFlights.stream().map(Flight::getId).collect(Collectors.toList())).containsExactly(flight1,
				flight2);
	}

}

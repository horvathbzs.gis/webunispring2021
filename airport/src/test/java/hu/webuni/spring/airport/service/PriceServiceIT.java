package hu.webuni.spring.airport.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles({ "prod", "test" })
public class PriceServiceIT {

	@Autowired
	PriceService priceService; // in IT service is injected with Spring

	@Test
	void testGetFinalPrice() throws Exception {

		int newPrice = priceService.getFinalPrice(100);
		assertThat(newPrice).isEqualTo(90);
	}

	@Test
	void testGetFinalPriceWithHighPrice() throws Exception {

		// --- with no @ActiveProfiles annotation this test uses SpecialDiscountService
		// because profile:prod ---
		// --- @ActiveProfiles prod and test, so setup comes from
		// application-test.properties, but uses SpecialDiscountService if needed ---
		int newPrice = priceService.getFinalPrice(11000);
		assertThat(newPrice).isEqualTo(9350);
	}
}

package hu.webuni.spring.airport.service;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import hu.webuni.spring.airport.model.LogEntry;
import hu.webuni.spring.airport.repository.LogEntryRepository;

@Service
public class LogEntryService {

	@Autowired
	LogEntryRepository logEntryRepository;

	/*
	 * bejelentkezes mar megtortent a web retegben igy csak a
	 * SecurityContextHolderbol el lehet kerni a username-et
	 */
	public void createLog(String description) {
		// callBackendSystem();
		logEntryRepository
				.save(new LogEntry(description, SecurityContextHolder.getContext().getAuthentication().getName()));
	}

	/**
	 * csak modellezzük, hogy minden 3. hívás sikertlene, azaz nem sikerül a
	 * LogEntry adatbázisba írása
	 */
	private void callBackendSystem() {
		if (new Random().nextInt(4) == 1)
			throw new RuntimeException();

	}
}

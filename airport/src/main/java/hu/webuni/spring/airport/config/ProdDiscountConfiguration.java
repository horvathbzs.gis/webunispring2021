package hu.webuni.spring.airport.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import hu.webuni.spring.airport.service.DiscountService;
import hu.webuni.spring.airport.service.SpecialDiscountService;

@Configuration
@Profile("prod")
public class ProdDiscountConfiguration {
    
    @Bean
    public DiscountService discountService() {
        return new SpecialDiscountService();
    }
}
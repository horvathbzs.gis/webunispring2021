package hu.webuni.spring.airport.service;

import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.data.jpa.domain.Specification;

import hu.webuni.spring.airport.model.Airport_;
import hu.webuni.spring.airport.model.Flight;
import hu.webuni.spring.airport.model.Flight_;

public class FlightSpecifications {

	/*
	 * Használja a JPA modelgeneratort (pom.xml-ben plugins). Így a root.get()
	 * paramétere lehet az Flight_.id)
	 * 
	 */
	public static Specification<Flight> hasId(long id) {
		return (root, cq, cb) -> cb.equal(root.get(Flight_.id), id);
	}

	/*
	 * Like keresés a flightnumberben
	 * 
	 */
	public static Specification<Flight> hasFlightNumber(String flightNumber) {
		return (root, cq, cb) -> cb.like(root.get(Flight_.flightNumber), flightNumber + "%");
	}

	/*
	 * felszállás időpontjának napja 0:00 beállítva, és a felszállás után egy nappal
	 * későbbi időpont közötti értékeket keresi
	 * 
	 */
	public static Specification<Flight> hasTakeoffTime(LocalDateTime takeoffTime) {
		LocalDateTime startOfDay = LocalDateTime.of(takeoffTime.toLocalDate(), LocalTime.of(0, 0));		
		return (root, cq, cb) -> cb.between(root.get(Flight_.takeoffTime), startOfDay, startOfDay.plusDays(1));
	}
	
	/*
	 * A Flight-ban tovább kell fűzni a takeoff(Airport)-nak az IATA-ja kell
	 */
	public static Specification<Flight> hasTakeoffIata(String takeoffIata) {
		return (root, cq, cb) -> cb.like(root.get(Flight_.takeoff).get(Airport_.iata), takeoffIata + "%");
	}
}

package hu.webuni.spring.airport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import hu.webuni.spring.airport.service.AirportService;
import hu.webuni.spring.airport.service.InitDbService;
import hu.webuni.spring.airport.service.PriceService;

@SpringBootApplication
public class AirportApplication implements CommandLineRunner {

	@Autowired
	PriceService priceService;
	
	@Autowired
	AirportService airportService;
	
	@Autowired
	InitDbService initDbService;

	public static void main(String[] args) {
		SpringApplication.run(AirportApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//airportService.createFlight();
		System.out.println(priceService.getFinalPrice((200)));
		System.out.println(priceService.getFinalPrice((20000)));
		initDbService.createUsersIfNeeded();
	}
}

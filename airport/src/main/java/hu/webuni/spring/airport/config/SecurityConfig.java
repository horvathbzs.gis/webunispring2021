package hu.webuni.spring.airport.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import hu.webuni.spring.airport.security.JwtAuthFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	UserDetailsService userDetailService;
	
	@Autowired
	JwtAuthFilter jwtAuthFilter;
	
	/*
	 * kodolja a passwordot inMemoryAutheticationben be kell allitani
	 */
	@Bean
	public PasswordEncoder passworEncoder() {
		return new BCryptPasswordEncoder();
	}

	/*
	 * user configuralas
	 * dao authentication providerhez kell UserDetailService
	 * nem inMemory-val, hanem authenticationProvider methoddal
	 * 
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.authenticationProvider(authenticationProvider());
		
//		auth.inMemoryAuthentication()
//			.passwordEncoder(passworEncoder())
//			.withUser("user").authorities("user").password(passworEncoder().encode("pass"))
//			.and()
//			.withUser("admin").authorities("user", "admin").password(passworEncoder().encode("pass"));

	}

	/*
	 * szabalyok configuralasa airports POST csak admin lehet airports PUT user
	 * jogosultsag is eleg minden egyebre kell bejelentkezes
	 * SessionCreationPolicy.STATELESS ->
	 * minden keresben ott kell utaznia a user-pass parosnak, mindig megtortenik az authentikacio
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
//			.httpBasic()
//			.and()
			.csrf().disable()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.authorizeRequests()
			.antMatchers("api/login/**").permitAll()
			.antMatchers(HttpMethod.POST, "api/airports/**").hasAuthority("admin")
			.antMatchers(HttpMethod.PUT, "api/airports/**").hasAnyAuthority("admin", "user")
			.anyRequest().authenticated();
		
		http.addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);
	}
	
	/*
	 * beallitunk minden, amit elozoben az auth.inMemoryAuthentication-ben
	 */
	@Bean
	public AuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
		daoAuthenticationProvider.setPasswordEncoder(passworEncoder());
		daoAuthenticationProvider.setUserDetailsService(userDetailService);
		return daoAuthenticationProvider;
	}
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

}

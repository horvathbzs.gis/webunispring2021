package hu.webuni.spring.airport.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.webuni.spring.airport.model.AirportUser;

public interface UserRepository extends JpaRepository<AirportUser, String> {

}

package hu.webuni.spring.airport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import hu.webuni.spring.airport.model.Flight;


/**
 * @author balazs.horvath
 * 
 *         JpaSpecificatorExecutor kell, hogy lehessen használni a Flight
 *         Specifications-ös megoldásokat
 *
 */
public interface FlightRepository extends JpaRepository<Flight, Long>, JpaSpecificationExecutor<Flight> {

}

package hu.webuni.spring.airport.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import hu.webuni.spring.airport.dto.AirportDto;
import hu.webuni.spring.airport.model.Airport;

@Mapper(componentModel = "spring")
public interface AirportMapper {

	List<AirportDto> airportsToDtos(List<Airport> airports);

	AirportDto airportToDto(Airport airport);

	Airport dtoToAirport(AirportDto airportDto);
	
}


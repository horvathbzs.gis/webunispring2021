package hu.webuni.spring.airport.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.webuni.spring.airport.model.LogEntry;

public interface LogEntryRepository extends JpaRepository<LogEntry, Long> {

}

package hu.webuni.spring.airport.web;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.web.reactive.server.WebTestClient;

import hu.webuni.spring.airport.dto.AirportDto;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AirportControllerIT {

	private static final String BASE_URI = "/api/airports";

	@Autowired
	WebTestClient webTestClient;
	// --- knows the port, and sends the request ---
	// --- webflux dependency must be added to pom.xml ---

	@Test
	void testThatCreatedAirportIsListed() throws Exception {
		List<AirportDto> airportsBefore = getAllAirpots();

		AirportDto newAirport = new AirportDto(5, "Test Airport", "TAP");
		createAirpot(newAirport);

		List<AirportDto> airportsAfter = getAllAirpots();

		// --- check the list before new list item added, get a sublist without the element
		// index lastly added ---
		// --- usingRecursiveFieldByFieldElementBomparator: not by reference, but by fields ---
		assertThat(airportsAfter.subList(0, airportsBefore.size()))
			.usingFieldByFieldElementComparator()
			.containsExactlyElementsOf(airportsBefore);

		// --- the item on the last index must be equal to the newly created element ---
		// --- usingRecursiveComparison: equal not by reference, but by fields ---
		assertThat(airportsAfter.get(airportsAfter.size() - 1)).usingRecursiveComparison().isEqualTo(newAirport);
	}

	private void createAirpot(AirportDto newAirport) {
		webTestClient.post().uri(BASE_URI).bodyValue(newAirport).exchange().expectStatus().isOk();

	}

	private List<AirportDto> getAllAirpots() {
		List<AirportDto> responseList = webTestClient.get().uri(BASE_URI).exchange().expectStatus().isOk().expectBodyList(AirportDto.class)
				.returnResult().getResponseBody();
		
		// --- list must be sorted, Comparator is not written -> adding compare method wiht lambda ---
		Collections.sort(responseList, (a1, a2) -> Long.compare(a1.getId(), a2.getId()));
		return responseList;
	}

}

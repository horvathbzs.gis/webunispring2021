package hu.webuni.spring.airport.service;

public interface DiscountService {
    
public int getDiscountPercent(int totalPrice);

}
package hu.webuni.spring.airport.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import hu.webuni.spring.airport.model.Airport;
import hu.webuni.spring.airport.model.Flight;
import hu.webuni.spring.airport.repository.AirportRepository;
import hu.webuni.spring.airport.repository.FlightRepository;

@Service
public class AirportService {

	// --- no @Autowired, but using constructor ---
	AirportRepository airportRepository;
	FlightRepository flightRepository;
	LogEntryService logEntryService;

	public AirportService(AirportRepository airportRepository, FlightRepository flightRepository,
			LogEntryService logEntryService) {
		super();
		this.airportRepository = airportRepository;
		this.flightRepository = flightRepository;
		this.logEntryService = logEntryService;
	}

	@Transactional
	public Airport save(Airport airport) {
		checkUniqueIata(airport.getIata(), null);

		return airportRepository.save(airport);
	}

	public List<Airport> findAll() {
		return airportRepository.findAll();
	}

	public Optional<Airport> findById(long id) {
		return airportRepository.findById(id);
	}

	/*
	 * Transactional kell legyen ez a metódus, mert különben a két adatbázis
	 * beszúrás független marad egymástól
	 * 
	 */
	@Transactional
	public Airport update(Airport airport) {
		checkUniqueIata(airport.getIata(), airport.getId());
		if (airportRepository.existsById(airport.getId())) {
			logEntryService.createLog(
					String.format("Airport modified with %d, new name is %s", airport.getId(), airport.getName()));
			callBackendSystem();
			return airportRepository.save(airport); // save: persist, or merge
		} else {
			throw new NoSuchElementException();
		}
	}

	@Transactional
	public void delete(long id) {
		airportRepository.deleteById(id);
	}

	@Transactional
	public Flight createFlight(String flightNumber, long takeoffAirportId, long landingAirportId,
			LocalDateTime takoffDateTime) {
		Flight flight = new Flight();
		flight.setFlightNumber(flightNumber);
		flight.setTakeoff(airportRepository.findById(takeoffAirportId).get());
		flight.setLanding(airportRepository.findById(landingAirportId).get());
		flight.setTakeoffTime(takoffDateTime);
		flightRepository.save(flight);

		return flight;
	}

	/**
	 * Egy specificationt építünk. A specificationben attól függően, hogy mi lesz
	 * kitöltve, generáljuk a lekérdezéseket (azaz a predicate-eket). Minden egyes
	 * szűrésre külön method-ot hozunk létre, és azokat fűzzük össze ÉS
	 * kapcsolattal. Ehhez a FlightRepository-nak be kell adni még egy extendet
	 * 
	 * @param example egy példa
	 * @return összefűzött predicate-ek, id szerint rendezve
	 */
	public List<Flight> findFlightsByExample(Flight example) {
		long id = example.getId();
		String flightNumber = example.getFlightNumber();
		String takeoffIata = null;
		Airport takeoff = example.getTakeoff();
		if (takeoff != null)
			takeoffIata = takeoff.getIata();
		LocalDateTime takeoffTime = example.getTakeoffTime();

		Specification<Flight> spec = Specification.where(null);

		if (id > 0) {
			spec = spec.and(FlightSpecifications.hasId(id));
		}

		if (StringUtils.hasText(flightNumber))
			spec = spec.and(FlightSpecifications.hasFlightNumber(flightNumber));

		if (StringUtils.hasText(takeoffIata)) {
			spec = spec.and(FlightSpecifications.hasTakeoffIata(takeoffIata));
		}

		if (takeoffTime != null) {
			spec = spec.and(FlightSpecifications.hasTakeoffTime(takeoffTime));
		}

		return flightRepository.findAll(spec, Sort.by("id"));
	}

	private void checkUniqueIata(String iata, Long id) {

		// --- if this method is used for save, param iata is used ---
		// --- if this method is used for update, param id also need to be used ---

		boolean forUpdate = id != null;

		Long count = forUpdate ? airportRepository.countByIataAndIdNot(iata, id) : airportRepository.countByIata(iata);

		if (count > 0)
			throw new NonUniqueIataException(iata);
	}

	/**
	 * csak modellezzük, hogy minden 3. hívás sikertlene, azaz nem sikerül a
	 * LogEntry adatbázisba írása
	 */
	private void callBackendSystem() {
		if (new Random().nextInt(4) == 1)
			throw new RuntimeException();

	}

	// --- PERSISTENCE CONTEXT ---
//	
//	@PersistenceContext
//	EntityManager em;
//
//	@Transactional
//	public Airport save(Airport airport) {
//		checkUniqueIata(airport.getIata(), null);
//		em.persist(airport);
//		;
//
//		return airport;
//	}
//
//	public List<Airport> findAll() {
//		return em.createQuery("SELECT a FROM Airport a", Airport.class).getResultList();
//	}
//
//	public Airport findById(long id) {
//		return em.find(Airport.class, id);
//	}
//
//	@Transactional
//	public Airport update(Airport airport) {
//		checkUniqueIata(airport.getIata(), airport.getId());
//		em.merge(airport);
//		return airport;
//	}
//
//	@Transactional
//	public void delete(long id) {
//		em.remove(findById(id));
//	}
//
//	private void checkUniqueIata(String iata, Long id) {
//
//		// --- if this method is used for save, param iata is used ---
//		// --- if this method is used for update, param id also need to be used ---
//		
//		boolean forUpdate = id != null;
//		TypedQuery<Long> query = em.createNamedQuery(forUpdate ? "Airport.countByIataAndIdNotIn" : "Airport.countByIata", Long.class)
//				.setParameter("iata", iata);
//		
//		if(forUpdate)
//			query.setParameter("id", id);
//		
//		Long count = query.getSingleResult();
//
//		if (count > 0)
//			throw new NonUniqueIataException(iata);
//	}

	// --- NOT USING DATABASE ---

//	@Autowired
//	AirportMapper airportMapper;
//
//	private Map<Long, Airport> airports = new HashMap<>();
//
//	{
//		airports.put(1L, new Airport(1L, "abc", "XYZ"));
//		airports.put(2L, new Airport(2L, "def", "UVW"));
//	}
//	
//	public Airport save(Airport airport) {
//		checkUniqueIata(airport.getIata());
//		airports.put(airport.getId(), airport);
//		
//		return airport;
//	}
//	
//	public List<Airport> findAll() {
//		return new ArrayList<> (airports.values());
//	}
//	
//	public Airport findById(long id) {
//		return airports.get(id);
//	}
//	
//	public ResponseEntity<AirportDto> modifyAirport(@PathVariable @Valid long id, @RequestBody AirportDto airportDto) {
//		if (!airports.containsKey(id)) {
//			return ResponseEntity.notFound().build();
//		}
//
//		checkUniqueIata(airportDto.getIata());
//		Airport airport = airportMapper.dtoToAirport(airportDto);
//		
//		airportDto.setId(id);
//		airports.put(id, airport);
//		return ResponseEntity.ok(airportDto);
//	}
//	
//	public void delete(long id) {
//		airports.remove(id);
//	}
//	
//	private void checkUniqueIata(String iata) {
//		Optional<Airport> airportWithSameIata = airports.values().stream().filter(a -> a.getIata().equals(iata))
//				.findAny();
//		if(airportWithSameIata.isPresent())
//			throw new NonUniqueIataException(iata);
//
//	}

}

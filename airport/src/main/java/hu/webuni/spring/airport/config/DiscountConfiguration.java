package hu.webuni.spring.airport.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import hu.webuni.spring.airport.service.DefaultDiscountService;
import hu.webuni.spring.airport.service.DiscountService;

@Configuration
@Profile("!prod") //not prod
public class DiscountConfiguration {

    @Bean
    public DiscountService discountService() {
        return new DefaultDiscountService();
    }
}